FROM fedora:27

# crank@qcrypt.org, renat.dusaev@cern.ch
MAINTAINER Renat Dusaev

# Install basic packages necessary for Bob build tool and basic toolchain.
# First section is most basic layer, the second is for Gant4, thirs is for
# ROOT.
RUN dnf install -y sudo git gcc gcc-c++ cmake python3-virtualenv \
    python2-virtualenv boost-devel patch \
    \
    expat-devel qt-devel libX11-devel libXft-devel \
    libXpm-devel libXext-devel libXmu-devel \
    mesa-libGLU-devel mesa-libGL-devel motif-devel xerces-c-devel \
    \
    openssl-devel pcre-devel glew-devel ftgl-devel mysql-devel graphviz-devel \
    avahi-compat-libdns_sd-devel openldap-devel python3-numpy libxml2-devel \
    gsl-devel

#
# The 'collector' is a default user in system, responsible for operations
# in userspace like configuring and building the software components.
RUN useradd --comment 'Source maintaining user.' \
    --create-home --home-dir /var/src \
    --user-group \
    --shell /bin/bash \
    collector \
 && echo "collector ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/collector \
 && chmod 0440 /etc/sudoers.d/collector

#
# Set defaults for ready image: default user, its working directory, and shared
# volume for placing the volatile data (e.g. source code of an active project):
USER collector
WORKDIR /var/src

#
# Set up Bob build tool. We're using virtualenv here since Fedora's system
# python schema package version is not compatible with the Bob.
RUN sudo mkdir /opt/bob \
 && sudo chown collector:collector /opt/bob \
 && git clone https://github.com/BobBuildTool/bob.git /opt/bob \
 && cd /opt/bob \
 && virtualenv-3 --prompt "venv-3\n" ./.venv \
 && ( . /opt/bob/.venv/bin/activate ; pip install PyYAML schema pyparsing ) \
 && ( . /opt/bob/.venv/bin/activate ; make) \
 && sudo /bin/sh -c "( . /opt/bob/.venv/bin/activate ; make install )"

#COPY project.bob /var/src/project.bob

#
# Installing Geant4

#
#RUN . /opt/bob/.venv/bin/activate \
# && cd /var/src/project.bob \
# && bob build geant4

