#!/bin/sh

BCKTTL="sV-resources secrets"

greetingsStr="This script will generate an secret information \
guiding you through few steps necessary for running micro-service \
environment. The input information shall be considered as unique data \
that is not supposed to be distributed among instances.\n\
\n\
Proceed?"

dialog --title 'The sV-resources secrets' \
       --backtitle "$BCKTTL" \
       --yesno "$greetingsStr" 12 60

case "$?" in
	'1')
	exit
	;;
	'-1')
	echo 'Failure.'
	exit 1
	;;
    # ... otherwise just continue...
esac

dbCredStr="Input database credentials. Use arrows to move between fields. DO \
NOT press <Enter> unless all the fields are filled."
dbInput=$(tempfile 2>/dev/null)
trap "rm -f $dbInput" 0 1 2 5 15
while : ; do
    dialog --title "Database Credentials" \
        --backtitle "$BCKTTL" \
        --clear \
        --insecure \
        --visit-items \
        --default-item "       password" \
        --ok-label 'Save' \
        --mixedform "$dbCredStr" 12 60 0 \
        "           user" 1 1 "p348data" 1 20 15 0 0 \
        "       password" 2 1 ""         2 20 15 0 1 \
        "password repeat" 3 1 ""         3 20 15 0 1 \
        2> $dbInput
    if [ $? -eq 1 ] ; then
        exit
    fi
    #readarray inp < $dbInput  # introduced in bash 4
    IFS=$'\n' read -d '' -r -a inp < $dbInput
    if [ "${#inp[@]}" -ne 3 ] ; then
        dbCredStr="All fields have to be filled!"
        continue
    fi
    if [ "${inp[1]}" != "${inp[2]}" ]; then
        dbCredStr="Password fields mismatch. Please, repeat input."
        continue
    fi
    echo "${inp[0]}" > ./db_user
    chmod 0600 ./db_user
    echo "${inp[1]}" > ./db_pw
    chmod 0600 ./db_pw
    break
done

siteSecretKey=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9~!@#$%^&*()' | fold -w 24 | head -n 1)
echo "$siteSecretKey" >> ./sV_rsrc_secret_key
chmod 0600 ./sV_rsrc_secret_key



